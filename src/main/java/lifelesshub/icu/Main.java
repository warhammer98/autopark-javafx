package lifelesshub.icu;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import lifelesshub.icu.config.AppContext;
import lifelesshub.icu.controller.ReportMenuController;
import lifelesshub.icu.controller.TableMenuController;
import lifelesshub.icu.entity.*;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Properties;

public class Main extends Application {

    private Stage primaryStage;
    private AnchorPane rootLayout;
    private AnchorPane loginLayout;

    @Override
    public void start(Stage primaryStage) {
        try {
            this.primaryStage = primaryStage;
            this.primaryStage.setTitle("Search in files");

            initLoginMenu();
            //initRootLayout();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }

    public String hashPassword(String password)
            throws NoSuchAlgorithmException {
        String hash = "35454B055CC325EA1AF2126E27707052";

        String md5Hex = DigestUtils
                .md5Hex(password).toUpperCase();

        return md5Hex;
    }

    private void loginButtonClick() {
        try {
            TextField usernameField = (TextField) loginLayout.getChildren().get(0);
            TextField passwordField = (TextField) loginLayout.getChildren().get(1);

            User user = AppContext.getUserRepository().findByUsername(usernameField.getText()).stream().findFirst().orElseThrow();
            String passwordHash = hashPassword(passwordField.getText());
            if (user.getPasswordHash().equals(passwordHash)) {
                initRootLayout();
            } else {
                System.out.println("Incorrect password");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initLoginMenu() throws IOException {
        loginLayout = FXMLLoader.load(getClass().getResource("login_menu.fxml"));
        Button loginButton = (Button) loginLayout.getChildren().get(2);
        loginButton.setOnAction(actionEvent -> loginButtonClick());

        Scene scene = new Scene(loginLayout);
        scene.getStylesheets().add(getClass().getResource("main.css").toString());
        primaryStage.setScene(scene);
        primaryStage.setHeight(500);
        primaryStage.setWidth(750);
        primaryStage.show();
    }

    private void initRootLayout() throws IOException {
        rootLayout = FXMLLoader.load(getClass().getResource("root.fxml"));
        Scene scene = new Scene(rootLayout);
        scene.getStylesheets().add(getClass().getResource("main.css").toString());
        primaryStage.setScene(scene);
        primaryStage.setHeight(500);
        primaryStage.setWidth(750);
        primaryStage.show();
        loadPersonnelMenu();
        loadAutoMenu();
        loadRouteMenu();
        loadJournalMenu();
        loadReportsMenu();
    }

    private void loadTableMenu(String fxml, String tabTitle) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource(fxml));
        AnchorPane mainMenu = loader.load();
        TabPane tabPane = (TabPane) rootLayout.getChildren().get(0);
        tabPane.getTabs().add(new Tab(tabTitle, mainMenu));
        TableMenuController tableMenuController = loader.getController();
        tableMenuController.setMain(this);
        tableMenuController.init();
    }

    private void loadReportsMenu() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("reports_menu.fxml"));
        AnchorPane mainMenu = loader.load();
        TabPane tabPane = (TabPane) rootLayout.getChildren().get(0);
        tabPane.getTabs().add(new Tab("Reports", mainMenu));
        ReportMenuController controller = loader.getController();
        controller.init();
        controller.setPrimaryStage(primaryStage);
    }

    private void loadRouteMenu() throws IOException {
        loadTableMenu("route_menu.fxml", "Route");
    }

    private void loadJournalMenu() throws IOException {
        loadTableMenu("journal_menu.fxml", "Journal");
    }

    private void loadAutoMenu() throws IOException {
        loadTableMenu("auto_menu.fxml", "Auto");
    }

    private void loadPersonnelMenu() throws IOException {
        loadTableMenu("personnel_menu.fxml", "Personnel");
    }

    public static void main(String[] args) {

        try {
            URL resource = Main.class.getClassLoader().getResource("autopark.properties");
            Properties properties = new Properties();
            properties.load(new FileInputStream(resource.getFile()));

            AppContext.create(properties);

            launch(args);

        } catch (Exception e) {
            e.printStackTrace();
        }



    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }
}
