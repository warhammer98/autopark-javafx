package lifelesshub.icu.repository;


import lifelesshub.icu.entity.JournalRecord;

public interface JournalRecordRepository extends BaseEntityRepository<JournalRecord, Integer> {

}
