package lifelesshub.icu.repository;


import lifelesshub.icu.entity.AutoPersonnel;

public interface AutoPersonnelRepository extends BaseEntityRepository<AutoPersonnel, Integer> {

}
