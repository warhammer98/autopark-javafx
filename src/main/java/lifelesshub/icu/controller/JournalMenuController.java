package lifelesshub.icu.controller;

import lifelesshub.icu.config.AppContext;
import lifelesshub.icu.entity.BaseEntity;
import lifelesshub.icu.entity.JournalRecord;
import lifelesshub.icu.entity.Route;
import lifelesshub.icu.repository.BaseEntityRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JournalMenuController extends TableMenuController<JournalRecord> {

    @Override
    protected Class<JournalRecord> getEntityClass() {
        return JournalRecord.class;
    }

    @Override
    protected List<String> getFieldNames() {
        return new ArrayList<>(Arrays.asList("id", "timeOut", "timeIn", "autoId", "routeId"));
    }

    @Override
    protected BaseEntityRepository<JournalRecord, Integer> getRepository() {
        return AppContext.getJournalRecordRepository();
    }

}
