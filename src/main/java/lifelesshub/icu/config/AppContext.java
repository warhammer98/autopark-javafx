package lifelesshub.icu.config;

import lifelesshub.icu.repository.*;
import org.sql2o.Sql2o;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.util.Properties;


public class AppContext {

    private static Sql2o sql2o;

    private static AutoRepository autoRepository;
    private static JournalRecordRepository journalRecordRepository;
    private static RouteRepository routeRepository;
    private static AutoPersonnelRepository personnelRepository;
    private static UserRepository userRepository;

    public static void create(Properties properties) {
        sql2o = new Sql2o(properties.getProperty("autopark.database.url"),
                properties.getProperty("autopark.database.user"),
                properties.getProperty("autopark.database.pass"));

        autoRepository = createAutoRepository(sql2o);
        personnelRepository = createAutoPersonnelRepository(sql2o);
        routeRepository = createRouteRepository(sql2o);
        journalRecordRepository = createJournalRecordRepository(sql2o);
        userRepository = createUserRepository(sql2o);

    }

    public static Sql2o getSql2o() {
        return sql2o;
    }

    public static UserRepository getUserRepository() {
        return userRepository;
    }

    public static AutoRepository getAutoRepository() {
        return autoRepository;
    }

    public static JournalRecordRepository getJournalRecordRepository() {
        return journalRecordRepository;
    }

    public static RouteRepository getRouteRepository() {
        return routeRepository;
    }

    public static AutoPersonnelRepository getPersonnelRepository() {
        return personnelRepository;
    }

    public static AutoRepository createAutoRepository(Sql2o sql2o) {
        return initRepositoryBean(AutoRepository.class, sql2o);
    }

    public static UserRepository createUserRepository(Sql2o sql2o) {
        return initRepositoryBean(UserRepository.class, sql2o);
    }
    public static JournalRecordRepository createJournalRecordRepository(Sql2o sql2o) {
        return initRepositoryBean(JournalRecordRepository.class, sql2o);
    }
    public static RouteRepository createRouteRepository(Sql2o sql2o) {
        return initRepositoryBean(RouteRepository.class, sql2o);
    }
    public static AutoPersonnelRepository createAutoPersonnelRepository(Sql2o sql2o) {
        return initRepositoryBean(AutoPersonnelRepository.class, sql2o);
    }

    public static <R> R initRepositoryBean(Class<R> repositoryClass, Sql2o sql2o) {
        ParameterizedType type = (ParameterizedType) repositoryClass.getGenericInterfaces()[0];
        Class entityClass = (Class) type.getActualTypeArguments()[0];
        return (R) Proxy.newProxyInstance(
               repositoryClass.getClassLoader(),
                new Class[] {repositoryClass},
                new RepositoryInvocationHandler(sql2o, entityClass));
    }

}
