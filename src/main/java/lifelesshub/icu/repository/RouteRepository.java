package lifelesshub.icu.repository;


import lifelesshub.icu.entity.Route;

public interface RouteRepository extends BaseEntityRepository<Route, Integer> {

}
