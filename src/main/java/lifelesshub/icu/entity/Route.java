package lifelesshub.icu.entity;

import lifelesshub.icu.util.GridColumn;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity(name = "routes")
@Table(schema = "autopark", name = "routes")
public class Route extends BaseEntity<Integer> {

    @Column(name = "name")
    @GridColumn(widthPercents = 87)
    protected String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
