package lifelesshub.icu.entity;

import lifelesshub.icu.util.ForeignKey;
import lifelesshub.icu.util.GridColumn;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity(name = "auto")
@Table(schema = "autopark", name = "auto")
public class Auto extends BaseEntity<Integer> {

    @Column(name = "num")
    @GridColumn(widthPercents = 15)
    protected String num;

    @Column(name = "color")
    @GridColumn(widthPercents = 10)
    private String color;

    @Column(name = "mark")
    @GridColumn(widthPercents = 30)
    private String mark;

    @Column(name = "personnel_id")
    @GridColumn(widthPercents = 30)
    @ForeignKey(table = AutoPersonnel.class)
    private Integer personnelId;

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public Integer getPersonnelId() {
        return personnelId;
    }

    public void setPersonnelId(Integer personnelId) {
        this.personnelId = personnelId;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s", num, color, mark);
    }
}
