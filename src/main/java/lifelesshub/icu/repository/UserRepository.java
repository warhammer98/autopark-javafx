package lifelesshub.icu.repository;


import lifelesshub.icu.entity.User;

import java.util.Collection;

public interface UserRepository extends BaseEntityRepository<User, Integer> {

    public Collection<User> findByUsername(String username);

}
