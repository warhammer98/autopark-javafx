package lifelesshub.icu.entity;


import lifelesshub.icu.util.GridColumn;

import javax.persistence.Column;
import javax.persistence.Id;
import java.util.Date;

public abstract class BaseEntity<ID> {

    @Id
    @Column(name = "id")
    @GridColumn(widthPercents = 3)
    protected ID id;

    @Column(name = "created")
    protected Date created;

    @Column(name = "updated")
    protected Date updated;

    public Boolean isPresent() {
        return id != null;
    }

    public void setId(ID id) {
        this.id = id;
    }

    public ID getId() {
        return id;
    }

}
