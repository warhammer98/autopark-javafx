package lifelesshub.icu.repository;

import lifelesshub.icu.util.EntityMetadata;
import lite.beans.Introspector;
import org.sql2o.Connection;
import org.sql2o.Query;
import org.sql2o.Sql2o;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.stream.Collectors;

public class RepositoryInvocationHandler<T, ID> implements InvocationHandler, BaseEntityRepository<T, ID> {

    private Sql2o sql2o;
    private EntityMetadata entityMetadata;
    private Class<T> entityClass;


    public RepositoryInvocationHandler(Sql2o sql2o, Class<T> clazz) {
        this.sql2o = sql2o;
        this.entityMetadata = new EntityMetadata(clazz);
        this.entityClass = clazz;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        String methodName = method.getName();

        boolean hasSuchMethod = Arrays.stream(this.getClass().getMethods())
                .anyMatch(m -> m.getName().equals(methodName));

        if (methodName.startsWith("findByNull") && !methodName.equals("findByNullField") && !hasSuchMethod) {
            String fieldName = Introspector.decapitalize(methodName.replace("findByNull", ""));
            Method findByField = this.getClass().getDeclaredMethod("findByNullField", String.class);
            return findByField.invoke(this, fieldName);
        }

        if (methodName.startsWith("findBy") && !methodName.equals("findByField") && !hasSuchMethod) {
            String fieldName = Introspector.decapitalize(methodName.replace("findBy", ""));
            Method findByField = this.getClass().getDeclaredMethod("findByField", String.class, Object.class);
            return findByField.invoke(this, fieldName, args[0]);
        }

        return method.invoke(this, args);
    }

    private List<T> select(String query, Map<String, Object> paramsMap) {
        query = query.replace("$table", entityMetadata.getSchemaAndTable());
        query = query.replace("$columnsList", String.join(", ", entityMetadata.getColumnsList()));
        try (Connection con = sql2o.open()) {
            Query q = con.createQuery(query);
            paramsMap.forEach(q::addParameter);
            return q.executeAndFetch(entityClass);
        }

    }

    private Boolean hasAnnotation(Method method, Class annotation) {
        return Objects.nonNull(method.getAnnotation(annotation));
    }

    private List<T> findByNullField(String field) {
        try (Connection con = sql2o.open()) {
            final String query =
                    String.format("SELECT * FROM %s WHERE %s IS NULL",
                            entityMetadata.getSchemaAndTable(),
                            entityMetadata.getColumnName(field));

            return con.createQuery(query)
                    .executeAndFetch(entityClass);
        }
    }

    private List<T> findByField(String field, Object value) {
        try (Connection con = sql2o.open()) {
            final String query =
                    String.format("SELECT * FROM %s WHERE %s = :value",
                            entityMetadata.getSchemaAndTable(),
                            entityMetadata.getColumnName(field));

            return con.createQuery(query)
                    .addParameter("value", value)
                    .executeAndFetch(entityClass);
        }
    }

    @Override
    public T findById(ID id) {
        return findByField("id", id).stream().findFirst().orElse(null);
    }

    @Override
    public List<T> findAll() {
        try (Connection con = sql2o.open()) {
            final String query =
                    String.format("SELECT * FROM %s", entityMetadata.getSchemaAndTable());

            return con.createQuery(query)
                    .executeAndFetch(entityClass);
        }
    }

    @Override
    public ID insert(T entity) {
        List<EntityMetadata.FieldMetadata> fields = entityMetadata.getNotNullFields(entity);
        final String insertQuery = String.format("INSERT INTO %s (%s) " + "VALUES (%s)",
                entityMetadata.getSchemaAndTable(),
                fields.stream().map(EntityMetadata.FieldMetadata::getColumnName)
                        .collect(Collectors.joining(", ")),
                fields.stream().map(f -> fieldToParam(f))
                        .collect(Collectors.joining(", ")));

        try (Connection con = sql2o.beginTransaction()) {
            Query bind = con.createQuery(insertQuery)
                    .bind(entity);

            ID key = (ID) bind.executeUpdate().getKey();
            con.commit();
            return key;
        }
    }

    @Override
    public ID update(T entity) {
        List<EntityMetadata.FieldMetadata> fields = entityMetadata.getNotNullFields(entity);
        final String insertQuery = String.format("UPDATE %s SET %s WHERE id = :id",
                entityMetadata.getSchemaAndTable(),
                fields.stream()
                        .filter(fieldMetadata -> !fieldMetadata.getFieldName().equals("id"))
                        .map(fieldMetadata -> fieldMetadata.getColumnName() +
                        " = " + fieldToParam(fieldMetadata))
                        .collect(Collectors.joining(", ")));

        try (Connection con = sql2o.beginTransaction()) {
            Query bind = con.createQuery(insertQuery)
                    .bind(entity);

                    bind.executeUpdate().getKey();
            con.commit();
            return null;
        }
    }

    @Override
    public void delete(ID id) {
        final String insertQuery = String.format("DELETE FROM %s WHERE id = :id",
                entityMetadata.getSchemaAndTable());

        try (Connection con = sql2o.beginTransaction()) {
            Query bind = con.createQuery(insertQuery)
                    .addParameter("id", id);

            bind.executeUpdate().getKey();
            con.commit();
        }
    }

    private String fieldToParam(EntityMetadata.FieldMetadata field) {
        return ":" + field.getFieldName();
    }
}
