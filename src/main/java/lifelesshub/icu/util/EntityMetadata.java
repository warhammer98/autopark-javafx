package lifelesshub.icu.util;

import lifelesshub.icu.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Table;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class EntityMetadata<T> {

    private Class<T> clazz;

    public EntityMetadata(Class<T> clazz) {
        this.clazz = clazz;
    }

    public String getColumnName(String fieldName) {
        Field field = findField(fieldName);
        Column column = field.getAnnotation(Column.class);
        return column.name();
    }

    public String getSchemaAndTable() {
        Table table = clazz.getAnnotation(Table.class);
        if (Objects.isNull(table)) {
            throw new RuntimeException("Entity class " + clazz.getName() + " has no 'Table' annotation");
        }
        return String.join(".", Arrays.asList(table.schema(), table.name()));
    }

    public List<FieldMetadata> getNotNullFields(T entity) {
        return getFields(entity).stream()
                .filter(fieldMetadata -> fieldMetadata.getValue() != null)
                .collect(Collectors.toList());
    }

    public List<FieldMetadata> getFields(T entity) {
        return findFields().stream()
                .filter(field -> field.getAnnotation(Column.class) != null)
                .map(field -> new FieldMetadata(field, entity))
                .collect(Collectors.toList());
    }

    public List<String> getColumnsList() {
        return findFields().stream()
                .filter(field -> field.getAnnotation(Column.class) != null)
                .map(field -> field.getAnnotation(Column.class).name())
                .collect(Collectors.toList());
    }

    private List<Field> findFields() {
        List<Field> fields = new ArrayList<>();
        Class currentClass = clazz;
        while (currentClass != null) {

            Field[] declaredFields = currentClass.getDeclaredFields();

            fields.addAll(Arrays.asList(declaredFields));

            currentClass = currentClass.getSuperclass();
        }
        return fields;
    }

    private Field findField(String fieldName) {
        Class currentClass = clazz;
        Field field = null;
        while (field == null && currentClass != null) {
            try {
                field = currentClass.getDeclaredField(fieldName);
            } catch (NoSuchFieldException e) {
                currentClass = currentClass.getSuperclass();
            }
        }
        return field;
    }

    public class FieldMetadata {

        private Field field;

        private Object value;

        public FieldMetadata(Field field, T entity) {
            try {
                field.setAccessible(true);
                this.field = field;
                this.value = field.get(entity);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public Field getField() {
            return field;
        }

        public Object getValue() {
            return value;
        }

        public String getColumnName() {
            return field.getAnnotation(Column.class).name();
        }

        public String getFieldName() {
            return field.getName();
        }

    }

    public static List<Field> findFields(Class clazz) {
        List<Field> fields = new ArrayList<>();
        Class currentClass = clazz;
        while (currentClass != null) {

            Field[] declaredFields = currentClass.getDeclaredFields();

            fields.addAll(Arrays.asList(declaredFields));

            currentClass = currentClass.getSuperclass();
        }
        return fields;
    }

    public static Field findField(String fieldName, Class clazz) {
        Class currentClass = clazz;
        Field field = null;
        while (field == null && currentClass != null) {
            try {
                field = currentClass.getDeclaredField(fieldName);
            } catch (NoSuchFieldException e) {
                currentClass = currentClass.getSuperclass();
            }
        }
        return field;
    }

    public static String entityToString(BaseEntity<Integer> entity) {
        List<Field> fields = EntityMetadata.findFields(entity.getClass());
        return fields.stream().filter(field -> field.getAnnotation(GridColumn.class) != null)
                .map(field -> {
                    try {
                        return field.getName() + "=" + field.get(entity);
                    } catch (Exception e) {
                        e.printStackTrace();
                        return "";
                    }
                }).collect(Collectors.joining(", "));
    }
}
