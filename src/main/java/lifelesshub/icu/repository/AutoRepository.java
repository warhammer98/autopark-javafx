package lifelesshub.icu.repository;


import lifelesshub.icu.entity.Auto;

public interface AutoRepository extends BaseEntityRepository<Auto, Integer> {

}
