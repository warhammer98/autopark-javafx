package lifelesshub.icu.controller;

import lifelesshub.icu.config.AppContext;
import lifelesshub.icu.entity.BaseEntity;
import lifelesshub.icu.entity.Route;
import lifelesshub.icu.repository.BaseEntityRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RouteMenuController extends TableMenuController<Route> {

    @Override
    protected Class<Route> getEntityClass() {
        return Route.class;
    }

    @Override
    protected List<String> getFieldNames() {
        return new ArrayList<>(Arrays.asList("id", "name"));
    }

    @Override
    protected BaseEntityRepository<Route, Integer> getRepository() {
        return AppContext.getRouteRepository();
    }
}
