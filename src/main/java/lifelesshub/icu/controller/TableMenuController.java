package lifelesshub.icu.controller;

import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import lifelesshub.icu.Main;
import lifelesshub.icu.config.AppContext;
import lifelesshub.icu.entity.Auto;
import lifelesshub.icu.entity.AutoPersonnel;
import lifelesshub.icu.entity.BaseEntity;
import lifelesshub.icu.entity.Route;
import lifelesshub.icu.repository.BaseEntityRepository;
import lifelesshub.icu.util.EntityMetadata;
import lifelesshub.icu.util.ForeignKey;
import lifelesshub.icu.util.GridColumn;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.*;

public abstract class TableMenuController<E extends BaseEntity<Integer>> {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");

    private Main main;
    private BaseEntityRepository<E, Integer> repository;

    @FXML
    public ScrollPane scrollPane;

    public GridPane gridPane;

    @FXML
    public Button newRowButton;

    public void init() {
        try {
            repository = getRepository();
            refreshData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void refreshData() {
        try {
            createGridPane();
            scrollPane.setContent(gridPane);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createGridPane() throws IllegalAccessException {
        gridPane = new GridPane();
        gridPane.setPrefWidth(scrollPane.getPrefWidth());
        //gridPane.setGridLinesVisible(true);

        double width = scrollPane.getPrefWidth();
        gridPane.setPrefWidth(width);

        List<String> fieldNames = getFieldNames();
        Class entityClass = getEntityClass();

        ColumnConstraints saveButtonC = new ColumnConstraints();
        saveButtonC.setPercentWidth(5);
        gridPane.getColumnConstraints().add(saveButtonC);

        ColumnConstraints deleteButtonC = new ColumnConstraints();
        deleteButtonC.setPercentWidth(5);
        gridPane.getColumnConstraints().add(saveButtonC);

        for (String fieldName : fieldNames) {
            Field field = EntityMetadata.findField(fieldName, entityClass);
            GridColumn gridColumn = field.getAnnotation(GridColumn.class);
            ColumnConstraints col = new ColumnConstraints();
            col.setHalignment(HPos.CENTER);
            col.setPercentWidth(gridColumn.widthPercents());
            gridPane.getColumnConstraints().add(col);
        }

        List<? extends BaseEntity<Integer>> data = repository.findAll();
        int i = 0;
        for (BaseEntity<Integer> object : data) {
            int j = 2;
            gridPane.addRow(i);

            List<Node> nodes = new ArrayList<>();
            for (String fieldName : fieldNames) {
                Field field = EntityMetadata.findField(fieldName, getEntityClass());
                GridColumn gridColumn = field.getAnnotation(GridColumn.class);
                ForeignKey foreignKey = field.getAnnotation(ForeignKey.class);
                field.setAccessible(true);

                Node node;
                if (foreignKey != null) {
                    node = createForeignSelector((Integer)field.get(object), foreignKey);
                } else if (fieldName.equals("id")){
                    node = new Text(field.get(object).toString());
                } else {
                    Object value = field.get(object);
                    if (value != null) {
                        if (field.getType().equals(Date.class)) {
                            node = new TextField(sdf.format((Date) value));
                        } else {
                            node = new TextField(field.get(object).toString());
                        }
                    } else {
                        node = new TextField();
                    }
                }

                gridPane.add(node, j++, i);
                nodes.add(node);
            }
            Button saveButton = createSaveButton(nodes);
            gridPane.add(saveButton, 0, i);
            Button deleteButton = createDeleteButton(object.getId());
            gridPane.add(deleteButton, 1, i);
            i++;
        }
    }

    private Button createDeleteButton(Integer entityId) {
        Button button = new Button("-");
        button.setOnAction(event -> {
            repository.delete(entityId);
            refreshData();
        });
        return button;
    }

    private Button createDeleteRowButton(Integer rowNumber) {
        Button button = new Button("-");
        button.setOnAction(event -> {
            gridPane.getChildren().removeIf(node -> GridPane.getRowIndex(node).equals(rowNumber));
        });
        return button;
    }

    private Map<Class, List<? extends BaseEntity<Integer>>> dataCache = new HashMap<>();

    private ChoiceBox<Object> createForeignSelector(Integer id, ForeignKey foreignKey) {
        ChoiceBox<Object> comboBox = new ChoiceBox<>();
        try {
            List<? extends BaseEntity<Integer>> foreignData;
            if (dataCache.containsKey(foreignKey.table())) {
                foreignData = dataCache.get(foreignKey.table());
            } else {
                BaseEntityRepository<? extends BaseEntity<Integer>, Integer> foreignRepository
                        = getForeignRepository(foreignKey.table());
                foreignData = foreignRepository.findAll();
                dataCache.put(foreignKey.table(), foreignData);
            }

            for (BaseEntity<Integer> object : foreignData) {
                comboBox.getItems().add(object);
                if (object.getId().equals(id))
                    comboBox.getSelectionModel().select(object);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return comboBox;
    }

    private Button createSaveButton(List<Node> nodes) {
        Button button = new Button("S");
        button.setOnAction(event -> {
            try {
                List<String> fieldNames = getFieldNames();
                Iterator<Node> iterator = nodes.iterator();

                Class<E> entityClass = getEntityClass();
                E object = entityClass.getConstructor().newInstance();

                for (String fieldName : fieldNames) {
                    Field field = EntityMetadata.findField(fieldName, entityClass);
                    field.setAccessible(true);
                    ForeignKey foreignKey = field.getAnnotation(ForeignKey.class);
                    Node node = iterator.next();

                    if (foreignKey != null) {
                        BaseEntity<Integer> value = (BaseEntity<Integer>) ((ChoiceBox<Object>) node).getValue();
                        field.set(object, value.getId());
                    } else if (fieldName.equals("id")) {
                        String value = ((Text) node).getText();
                        if (value.isEmpty()) continue;
                        if (field.getType().equals(Integer.class)) {
                            field.set(object, Integer.valueOf(value));
                        } else {
                            field.set(object, value);
                        }
                    } else {
                        String value = ((TextField) node).getText();
                        if (value.isEmpty()) continue;
                        if (field.getType().equals(Date.class)) {
                            field.set(object, sdf.parse(value));
                        } else {
                            field.set(object, value);
                        }
                    }
                }

                if (object.isPresent()) {
                    repository.update(object);
                } else {
                    repository.insert(object);
                }
                refreshData();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        return button;
    }

    @FXML
    public void newRowButtonClick() {
        List<String> fieldNames = getFieldNames();

        int i = gridPane.getRowCount();
        int j = 2;
        List<Node> nodes = new ArrayList<>();
        for (String fieldName : fieldNames) {
            Field field = EntityMetadata.findField(fieldName, getEntityClass());
            ForeignKey foreignKey = field.getAnnotation(ForeignKey.class);
            field.setAccessible(true);

            Node node;
            if (foreignKey != null) {
                node = createForeignSelector(null, foreignKey);
            } else if (fieldName.equals("id")) {
                node = new Text();
            } else {
                node = new TextField();
            }

            gridPane.add(node, j++, i);
            nodes.add(node);
        }
        Button saveButton = createSaveButton(nodes);
        gridPane.add(saveButton, 0, i);
        Button deleteRowButton = createDeleteRowButton(i);
        gridPane.add(deleteRowButton, 1, i);
    }

    private BaseEntityRepository<? extends BaseEntity<Integer>,Integer> getForeignRepository(Class<? extends BaseEntity<Integer>> clazz) {
        if (clazz.equals(AutoPersonnel.class)) {
            return AppContext.getPersonnelRepository();
        } else if (clazz.equals(Auto.class)) {
            return AppContext.getAutoRepository();
        } else if (clazz.equals(Route.class)) {
            return AppContext.getRouteRepository();
        }
        return null;
    }

    protected abstract BaseEntityRepository<E, Integer> getRepository();

    protected abstract Class<E> getEntityClass();

    protected abstract List<String> getFieldNames();

    public void setMain(Main main) {
        this.main = main;
    }

    public void setRepository(BaseEntityRepository<E, Integer> repository) {
        this.repository = repository;
    }
}
