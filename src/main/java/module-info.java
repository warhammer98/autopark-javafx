module lifelesshub.icu {
    requires javafx.controls;
    requires javafx.fxml;
    requires persistence.api;
    requires sql2o;
    requires com.github.panga.beans;
    requires org.apache.commons.codec;
    requires java.sql;
    exports lifelesshub.icu.entity;
    exports lifelesshub.icu.repository;
    exports lifelesshub.icu.controller;
    exports lifelesshub.icu;
    opens lifelesshub.icu.entity;
}
