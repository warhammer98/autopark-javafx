package lifelesshub.icu.entity;

import lifelesshub.icu.util.ForeignKey;
import lifelesshub.icu.util.GridColumn;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity(name = "journal")
@Table(schema = "autopark", name = "journal")
public class JournalRecord extends BaseEntity<Integer> {

    @Column(name = "time_out")
    @GridColumn(widthPercents = 20)
    protected Date timeOut;

    @Column(name = "time_in")
    @GridColumn(widthPercents = 20)
    protected Date timeIn;

    @Column(name = "auto_id")
    @GridColumn(widthPercents = 27)
    @ForeignKey(table = Auto.class)
    protected Integer autoId;

    @Column(name = "route_id")
    @GridColumn(widthPercents = 20)
    @ForeignKey(table = Route.class)
    protected Integer routeId;

    public Date getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(Date timeOut) {
        this.timeOut = timeOut;
    }

    public Date getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(Date timeIn) {
        this.timeIn = timeIn;
    }

    public Integer getAutoId() {
        return autoId;
    }

    public void setAutoId(Integer autoId) {
        this.autoId = autoId;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }
}
