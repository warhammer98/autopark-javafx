package lifelesshub.icu.controller;

import lifelesshub.icu.config.AppContext;
import lifelesshub.icu.entity.AutoPersonnel;
import lifelesshub.icu.entity.BaseEntity;
import lifelesshub.icu.repository.BaseEntityRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PersonnelMenuController extends TableMenuController<AutoPersonnel> {

    @Override
    protected Class<AutoPersonnel> getEntityClass() {
        return AutoPersonnel.class;
    }

    @Override
    protected List<String> getFieldNames() {
        return new ArrayList<>(Arrays.asList("id", "firstName", "lastName", "patherName"));
    }

    @Override
    protected BaseEntityRepository<AutoPersonnel, Integer> getRepository() {
        return AppContext.getPersonnelRepository();
    }
}
