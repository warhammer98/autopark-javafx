package lifelesshub.icu.entity;

import lifelesshub.icu.util.GridColumn;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity(name = "auto_personnel")
@Table(schema = "autopark", name = "auto_personnel")
public class AutoPersonnel extends BaseEntity<Integer> {

    @Column(name = "first_name")
    @GridColumn(widthPercents = 27)
    protected String firstName;

    @Column(name = "last_name")
    @GridColumn(widthPercents = 30)
    protected String lastName;

    @Column(name = "pather_name")
    @GridColumn(widthPercents = 30)
    protected String patherName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPatherName() {
        return patherName;
    }

    public void setPatherName(String patherName) {
        this.patherName = patherName;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s", lastName, firstName, patherName);
    }
}
