package lifelesshub.icu.controller;

import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import lifelesshub.icu.Main;
import lifelesshub.icu.config.AppContext;
import lifelesshub.icu.repository.AutoPersonnelRepository;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.List;

public class ReportMenuController {

    private Stage primaryStage;

    private Sql2o sql2o;
    private AutoPersonnelRepository personnelRepository;

    @FXML
    public DatePicker dateFrom;

    @FXML
    public DatePicker dateTo;

    @FXML
    public TextField paymentsSum;

    @FXML
    public GridPane gridPane;

    public void init() {
        this.sql2o = AppContext.getSql2o();
        this.personnelRepository = AppContext.getPersonnelRepository();

        dateFrom.setValue(LocalDate.of(2020, 11, 14));
        dateTo.setValue(LocalDate.of(2021, 1, 1));
        paymentsSum.setText("50000");
    }

    private List<PaymentResult> calculatePayments() {
        String query = "select * from prem(:dateFrom::date, :dateTo::date, :paymentsSum::integer)";
        try (final Connection connection = sql2o.open()) {
            return connection.createQuery(query)
                    .addParameter("dateFrom", dateFrom.getValue().toString())
                    .addParameter("dateTo", dateTo.getValue().toString())
                    .addParameter("paymentsSum", Integer.parseInt(paymentsSum.getText()))
                    .executeAndFetch(PaymentResult.class);
        }
    }

    @FXML
    public void saveToFile() {
        List<PaymentResult> paymentResults = calculatePayments();

        FileChooser fileChooser = new FileChooser();

        //Set extension filter for text files
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File file = fileChooser.showSaveDialog(primaryStage);

        if (file != null) {
            StringBuilder csv = new StringBuilder("Personnel;Wins count;Payments sum\n");
            for (PaymentResult result : paymentResults) {
                csv.append(String.format("%s;%s;%s\n",
                        personnelRepository.findById(result.getPid()).toString(),
                        result.getCount(),
                        result.getSum()));
            }

            saveTextToFile(csv.toString(), file);
        }
    }

    private void saveTextToFile(String content, File file) {
        try {
            PrintWriter writer;
            writer = new PrintWriter(file);
            writer.println(content);
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    @FXML
    public void printPaymentsResult() {


        int i = 1;
        for (PaymentResult result : calculatePayments()) {
            gridPane.add(new Text(personnelRepository.findById(result.getPid()).toString()), 0, i);
            gridPane.add(new Text(result.getCount().toString()), 1, i);
            gridPane.add(new Text(result.getSum() + " RUB"), 2, i);
            i++;
        }

        //return;
    }

    public static class PaymentResult {
        private Integer pid;
        private Integer count;
        private Integer sum;

        public Integer getPid() {
            return pid;
        }

        public void setPid(Integer pid) {
            this.pid = pid;
        }

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }

        public Integer getSum() {
            return sum;
        }

        public void setSum(Integer sum) {
            this.sum = sum;
        }
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }
}
