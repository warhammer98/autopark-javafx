package lifelesshub.icu.repository;

import java.util.List;

public interface BaseEntityRepository<T, ID> {

    T findById(ID id);

    List<T> findAll();

    ID insert(T entity);

    ID update(T entity);

    void delete(ID id);

}
