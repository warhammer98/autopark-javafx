package lifelesshub.icu.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity(name = "auto")
@Table(schema = "autopark", name = "users")
public class User extends BaseEntity<Integer> {

    @Column(name = "username")
    private String username;

    @Column(name = "password_hash")
    private String passwordHash;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }
}
