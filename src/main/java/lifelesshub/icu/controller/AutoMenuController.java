package lifelesshub.icu.controller;

import lifelesshub.icu.config.AppContext;
import lifelesshub.icu.entity.Auto;
import lifelesshub.icu.entity.AutoPersonnel;
import lifelesshub.icu.entity.BaseEntity;
import lifelesshub.icu.repository.BaseEntityRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AutoMenuController extends TableMenuController<Auto> {

    @Override
    protected Class<Auto> getEntityClass() {
        return Auto.class;
    }

    @Override
    protected List<String> getFieldNames() {
        return new ArrayList<>(Arrays.asList("id", "num", "color", "mark", "personnelId"));
    }

    @Override
    protected BaseEntityRepository<Auto, Integer> getRepository() {
        return AppContext.getAutoRepository();
    }
}
